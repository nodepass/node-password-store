const promisifyAll = require("bluebird").promisifyAll
const fs = promisifyAll(require("fs-extra"))
const path = require("path")

function isHiddenFile(mypath) {
	return path.basename(mypath).startsWith(".")
}

function safeIsDirectory(path) {
	return fs.lstatAsync(path)
		.then((stats) => stats.isDirectory())
		.catch(() => false)
}

function isFile(filename) {
	return fs.lstatAsync(filename)
		.then((stats) => stats.isFile())
}

function safeIsFile(filename) {
	return isFile(filename).catch(() => false)
}

function walk(dirpath) {
	return new Promise(function(resolve, reject) {
		let files = []
		fs.walk(dirpath)
			.on("data", (item) => { if (item.stats.isFile() && !isHiddenFile(item.path)) files.push(item.path) })
			.on("error", (err) => reject(err))
			.on("end", () => resolve(files))
	})
}

module.exports = {
	fs,
	safeIsDirectory,
	isFile,
	safeIsFile,
	walk
}
