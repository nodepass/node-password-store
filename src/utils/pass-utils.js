const gpg = require("gpg")
const path = require("path")

function checkBadPath(path) {
	return new Promise(function(resolve, reject) {
		if (isBadPath(path)) {
			reject(new Error("You've attempted to pass a sneaky path to pass. Go home."))
		} else {
			resolve()
		}
	})
}

function decryptFile(filename) {
	return new Promise(function(resolve, reject) {
		gpg.decryptFile(filename, function(err, contents) {
			if (err) return reject(err)
			resolve(contents.toString())
		})
	})
}

function fileToPass(passdir, files) {
	return files.map(function(file) {
		if (file.startsWith(passdir)) {
			file = file.slice(passdir.length)
		}
		if (file.startsWith(path.sep)) {
			file = file.slice(1)
		}
		if (file.endsWith(".gpg")) {
			file = file.slice(0, -4)
		}
		return file
	})
}

function isBadPath(path) {
	if (path) {
		if (/\/\.\.$/.test(path)      // Path ends in /..
				|| /^\.\.\//.test(path)   // Path starts with ../
				|| /\/\.\.\//.test(path)  // Path contains /../
				|| /^\.\.$/.test(path)) { // Path is ..
			return true
		}
	}
	return false
}

module.exports = {
	checkBadPath,
	decryptFile,
	fileToPass
}
