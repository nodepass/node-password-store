const _ = require("lodash/core")
const os = require("os")
const path = require("path")
const utils = require("./utils/pass-utils")
const futils = require("./utils/file-utils")

const PREFIX = path.normalize(process.env.PASSWORD_STORE_DIR ?
	process.env.PASSWORD_STORE_DIR : `${os.homedir()}/.password-store`)

function list(subfolder = "") {
	let passdir = path.join(PREFIX, subfolder)
	return utils.checkBadPath(subfolder)
		.then(() => futils.safeIsDirectory(passdir))
		.then(function(isDir) {
			if (!isDir) {
				if (!subfolder) {
					throw new Error("password store is empty.")
				} else {
					throw new Error(`${subfolder} is not in the password store.`)
				}
			}
		})
		.then(() => futils.walk(passdir))
		.then((files) => _.flattenDeep(files))
		.then((files) => utils.fileToPass(passdir, files))
}

function find(...names) {
	if (!names.length) return Promise.reject(new Error("name is required"))
	return list().then(function(files) {
		const matcher = new RegExp(names.join("|"))
		const results = []
		for (const file of files) {
			if (matcher.test(file)) {
				results.push(file)
			}
		}
		return results
	})
}

function show(filename = "") {
	utils.checkBadPath(filename)
	let passfile = `${path.join(PREFIX, filename)}.gpg`
	return futils.safeIsFile(passfile)
		.then(function(isFile) {
			if (isFile) {
				return utils.decryptFile(passfile)
			} else {
				// is it a directory?
				return list(filename)
			}
		})
}

module.exports = {
	list,
	find,
	show
}
