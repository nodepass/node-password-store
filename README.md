# Node Password Store [![Build Status](https://gitlab.com/nodepass/node-password-store/badges/master/build.svg)](https://gitlab.com/nodepass/node-password-store/pipelines) [![Known Vulnerabilities](https://snyk.io/test/npm/password-store/badge.svg)](https://snyk.io/test/npm/password-store) [![codecov](https://codecov.io/gl/nodepass/node-password-store/branch/master/graph/badge.svg?token=4ENYljFy89)](https://codecov.io/gl/nodepass/node-password-store)


An implementation of [pass](https://www.passwordstore.org/) the standard
unix password manager in node.js.

Node Password Store uses [Node-GPG](https://github.com/drudge/node-gpg) for
GPG encryption and decryption. Node-GPG is a wrapper for the `gpg` binary, so
be sure that it is available in your `$PATH`.

> WARNING: This is an alpha release, API changes _will_ happen before
> the final release.

## Installation

```
npm install password-store
```

## Usage

#### init()

> Needs implemented.

#### list(subfolder)

Lists the passnames in the password store or a specified subfolder.
Returns a Promise that resolves with an array of password filenames.

```js
Pass.list("test")
	.then((files) => console.log(files))
	.catch((err) => console.error(err.message))
```

#### grep()

> Needs implemented.

#### find(...names)

Lists the passnames that match the given name(s).
Returns a Promise that resolves with an array of password filenames
or an empty array if no matches were found.

```js
Pass.find("test", "foobar")
	.then((files) => console.log(files))
	.catch((err) => console.error(err.message))
```

#### show(passname)

Decrypts the gpg encrypted password file represented by `passname`.
Calls `list()` as a fallback if `passname` is  a directory or is not provided.
Returns a Promise that resolves with the decrypted file contents.

```js
Pass.show("test/example.com")
	.then((contents) => console.log(contents))
	.catch((err) => console.error(err.message))
```

#### insert()

> Needs implemented.

#### edit()

> Needs implemented.

#### generate()

> Needs implemented.

#### remove()

> Needs implemented.

#### move()

> Needs implemented.

#### copy()

> Needs implemented.

## License
Node Password Store is licensed under the [BSD 2-Clause License](./LICENSE).
