let Pass

describe("password-store", function() {
	beforeEach(function() {
		process.env.PASSWORD_STORE_DIR = require("path").resolve(`${__dirname}/empty-store`)
		Pass = require("../src/index")
	})

	describe("list()", function() {
		// Check empty store
		it("should throw an error for an empty store", function() {
			return Pass.list()
				.then(() => fail("no error was thrown"))
				.catch((e) => expect(e.message).toEqual("password store is empty."))
		})
	})
})
