let Pass

describe("password-store", function() {
	beforeEach(function() {
		process.env.PASSWORD_STORE_DIR = require("path").resolve(`${__dirname}/store`)
		Pass = require("../src/index")
	})

	describe("list()", function() {
		it("should list the passwords in ./store", function() {
			return Pass.list()
				.then(function(results) {
					expect(Array.isArray(results)).toBe(true)
					expect(results.length).toBe(2)
					expect(results[0]).toBe("test/example.com")
				})
		})
		// Check subfolder that does not exist
		it("should throw an error for a non existant subfolder", function() {
			return Pass.list("foobar")
				.then(() => fail("no error was thrown"))
				.catch((e) => expect(e.message).toBe("foobar is not in the password store."))
		})

		// Check bad paths
		it("should throw an error for paths ending with /..", function() {
			return Pass.list("foobar/..")
				.then(() => fail("no error was thrown"))
				.catch((e) => expect(e.message).toBe("You've attempted to pass a sneaky path to pass. Go home."))
		})
		it("should throw an error for paths starting with ../", function() {
			return Pass.list("../foobar")
				.then(() => fail("no error was thrown"))
				.catch((e) => expect(e.message).toBe("You've attempted to pass a sneaky path to pass. Go home."))
		})
		it("should throw an error for paths containing /../", function() {
			return Pass.list("foo/../bar")
				.then(() => fail("no error was thrown"))
				.catch((e) => expect(e.message).toBe("You've attempted to pass a sneaky path to pass. Go home."))
		})
		it("should throw an error for ..", function() {
			return Pass.list("..")
				.then(() => fail("no error was thrown"))
				.catch((e) => expect(e.message).toBe("You've attempted to pass a sneaky path to pass. Go home."))
		})
	})

	describe("find()", function() {
		it("should find a match", function() {
			return Pass.find("exam")
				.then(function(results) {
					expect(Array.isArray(results)).toBe(true)
					expect(results.length).toBe(1)
					expect(results[0]).toBe("test/example.com")
				})
		})

		it("should return an empty array if no match is found", function() {
			return Pass.find("foo", "bar")
				.then(function(results) {
					expect(Array.isArray(results)).toBe(true)
					expect(results.length).toBe(0)
				})
		})

		it("should throw an error if no name parameter is given", function() {
			return Pass.find()
				.then(() => fail("no error was thrown"))
				.catch((e) => expect(e.message).toBe("name is required"))
		})
	})

	describe("show()", function() {
		it("should return the contents of a file", function() {
			return Pass.show("test/example.com")
				.then(function(result) {
					expect(result).toBe("secretsquirrel\n")
				})
		})

		it("should call list() for a directory", function() {
			return Pass.show("test")
				.then(function(results) {
					expect(Array.isArray(results)).toBe(true)
					expect(results.length).toBe(2)
					expect(results[0]).toBe("example.com")
				})
		})
	})
})
