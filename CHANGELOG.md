# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/)
and this project adheres to [Semantic Versioning](http://semver.org/).

## Unreleased
### Fixed
- Add [required authentication](https://snyk.io/blog/requiring-authentication/) for Snyk

## 1.0.0-alpha.5 - 2016-12-01
### Added
- Add [Snyk](https://snyk.io) vulnerability scanning and badge
- Add a changelog
- Add node v7 to the test matrix

### Changed
- Move repository to the [nodepass group](https://gitlab.com/nodepass)

### Fixed
- Add workaround for Codecov detection of GitLab CI

## 1.0.0-alpha.4 - 2016-09-30
### Added
- Update jest version
- Add `git push` to postversion script to simplify publishing

### Changed
- Refactor file utilities

## 1.0.0-alpha.3 - 2016-07-08
### Added
- Add npm description

### Fixed
- Fix building on publish

## 1.0.0-alpha.2 - 2016-07-08
### Fixed
- Revert removing `src` directory from `.npmignore`

## 1.0.0-alpha.1 - 2016-07-08
- Initial published version
